//
//  Constants.swift
//  MyWeather
//
//  Created by iOS on 16/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation


class Constants {
    static let API_KEY = "5a3c8b16bd354f494baaa3dcc2436b62"
    static let ROOT_URL = "https://api.openweathermap.org"
    static let googleKeyMap = "AIzaSyA5jZMMNij0XxUvkkXzOl4F7gIJTJenh4k"
    
    static let  BASE_PARAMS : [String: Any] = [
        "api_key"            : Constants.API_KEY
    ]
    
}
