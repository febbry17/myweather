//
//  CustomButton.swift
//  MyWeather
//
//  Created by iOS on 17/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import Hue

@IBDesignable class CustomButton: UIButton{
    private var _pxEnabled = false
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI(){
        self.titleLabel?.font = FontFamily.Roboto.medium.font(size: 20)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitleColor(UIColor.white, for: .highlighted)
        self.setTitleColor(UIColor.white, for: .selected)
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)

        self.layer.shadowOpacity = 0.8
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 4
        self.layer.masksToBounds = false
        //
    }
    
    @IBInspectable var pxEnabled: Bool {
        set {
            _pxEnabled = newValue
            setEnable()
        }
        get {
            return self._pxEnabled
        }
    }
    
    private func setEnable() {
        if self._pxEnabled {
            self.isEnabled = true
            self.backgroundColor = UIColor(hex: "#0a66e2")
            self.setTitleColor(UIColor.white, for: .normal)
            self.setTitleColor(UIColor.white, for: .highlighted)
            self.setTitleColor(UIColor.white, for: .selected)
        }else{
            self.isEnabled = false
            self.backgroundColor = UIColor(hex: "#cdcdcd")
            self.setTitleColor(UIColor.darkGray, for: .normal)
            self.setTitleColor(UIColor.darkGray, for: .highlighted)
            self.setTitleColor(UIColor.darkGray, for: .selected)
        }
        
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    
    
}
