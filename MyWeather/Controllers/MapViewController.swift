//
//  MapViewController.swift
//  MyWeather
//
//  Created by iOS on 18/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    @IBOutlet weak var mapWrapper: UIView!
    @IBOutlet var zcontrol: ZoomControls!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var latitude: Double!
    var longitude: Double!
    var mainWeather: MainWeather!
    var marker = GMSMarker()
    var tappedMarker : GMSMarker?
    var gmapView: GMSMapView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // custom back button
//        navigationController?.navigationBar.backIndicatorImage = Asset.back.image
//        navigationController?.navigationBar.backIndicatorTransitionMaskImage = Asset.back.image
//        navigationController?.navigationBar.backItem?.title = " "
//        navigationController?.navigationBar.tintColor = .pxPurple
    }

    
    override func viewDidLoad() {
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 8.0)
        gmapView = GMSMapView.map(withFrame: mapWrapper.bounds, camera: camera)
        gmapView.settings.compassButton = true
        gmapView.settings.zoomGestures = false

        mapWrapper.addSubview(gmapView)
        gmapView.delegate = self
        zcontrol.mapView = gmapView
        marker.map = gmapView
        marker.tracksViewChanges = true
        
        getCurrentWeather()
    }
    
    func updateMarker(){
        let city_name = mainWeather.name ?? ""
        let weather_status = mainWeather.weather.first?.main ?? ""
        let temp = mainWeather.main.temp ?? 0
        let humidity = mainWeather.main.humidity ?? 0
        let wind_speed = mainWeather.wind.speed ?? 0
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.title = city_name
        //        marker.snippet = mainWeather.weather.first?.main ?? ""
        marker.snippet = String(format: "\nWeather: %@\nTemperature: %i\nWind Speed: %i\nHumidity: %i", weather_status, Int(temp), Int(wind_speed), humidity)
    }
    
    func getCurrentWeather(){
        indicatorView.startAnimating()
        if let lat = latitude, let lon = longitude {
            MainViewModel.getCurrentWeather(params: ["lat": lat, "lon": lon], success: { (result) in
                self.mainWeather = MainWeather(result)
                self.updateMarker()
                self.indicatorView.stopAnimating()
                
            }) { (error)  in
                print("error is \(error ?? "")")
                //            self.showAlert("Error", error!, "OK", false)
                self.indicatorView.stopAnimating()
                
            }
        }
        
    }
}


extension MapViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        latitude = coordinate.latitude
        longitude = coordinate.longitude

        getCurrentWeather()
    }
}
