//
//  NoLocationViewController.swift
//  MyWeather
//
//  Created by iOS on 17/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import CoreLocation

class NoLocationViewController: UIViewController {
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    let locationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        indicatorView.stopAnimating()
        
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func didTapTurnOnLocation(){
        
        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else
            {
                UIApplication.shared.openURL(url)
                // Fallback on earlier versions
            }
        }
    }
    
    func openDashboard(){
        let vc = StoryboardScene.Main.dashboard.instantiate()
        let navigationController = UINavigationController(rootViewController: vc)
        
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
}

extension NoLocationViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        indicatorView.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.openDashboard()
        } 
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locations error = \(error.localizedDescription)")
        indicatorView.stopAnimating()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var shouldIAllow = false

        switch status {
        case .restricted:
            print("Restricted Access to location")
        case .denied:
            print("User denied access to location")
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            print("Status not determined")
        default:
            print("Allowed to location Access")
            shouldIAllow = true
        }
        
        if (shouldIAllow) {
            NSLog("Location to Allowed")
            // Start location services
            locationManager.startUpdatingLocation()
        } else {
            NSLog("Denied access: \(status)")
        }
    }
}
