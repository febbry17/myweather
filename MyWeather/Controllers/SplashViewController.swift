//
//  SplashViewController.swift
//  MyWeather
//
//  Created by iOS on 16/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import Lottie
import CoreLocation


class SplashViewController: UIViewController {
    @IBOutlet weak var wrapperAnimation: UIView!
    @IBOutlet weak var versionLabel: UILabel!
    var hasLocation = false
    
    let animationView = AnimationView()
    let locationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        
        let animation = Animation.named("splash")
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        wrapperAnimation.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.centerXAnchor.constraint(equalTo: wrapperAnimation.centerXAnchor).isActive = true
        animationView.centerYAnchor.constraint(equalTo: wrapperAnimation.centerYAnchor).isActive = true
        
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionLabel.text = "Version: \(text)"
        }

        locationManager.requestWhenInUseAuthorization()

        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        animationView.play(fromProgress: 0,
                           toProgress: 1,
                           loopMode: LottieLoopMode.playOnce,
                           completion: { (finished) in
                            self.openDashboard()
        })
    }
    
    
    func openDashboard(){
        if hasLocation {
            let vc = StoryboardScene.Main.dashboard.instantiate()
            let navigationController = UINavigationController(rootViewController: vc)
            
            UIApplication.shared.keyWindow?.rootViewController = navigationController
        } else {
            let vc = StoryboardScene.Main.noLocation.instantiate()
            let navigationController = UINavigationController(rootViewController: vc)
            
            UIApplication.shared.keyWindow?.rootViewController = navigationController
        }
        
    }
}


extension SplashViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        hasLocation = true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locations error = \(error.localizedDescription)")
        hasLocation = false
    }
}
