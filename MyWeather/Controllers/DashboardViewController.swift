//
//  ViewController.swift
//  MyWeather
//
//  Created by iOS on 16/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import CoreLocation
import AlamofireImage


class DashboardViewController: UIViewController {
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var tempLabelFlag: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var wrapperMapButton: UIView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    
    var mainWeather: MainWeather!
    let locationManager = CLLocationManager()
    var latitude: Double!
    var longitude: Double!
    var isCelsius = true
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        isCelsius = (segmentView.selectedSegmentIndex == 0)
        getCurrentWeather()
    }
    
    @IBAction func didTapMap(){
        let vc = StoryboardScene.Main.mapView.instantiate()
        vc.latitude = latitude
        vc.longitude = longitude
        vc.mainWeather = mainWeather
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI(){
        var attributedString = NSMutableAttributedString()
        
        if let result = mainWeather {
            let city_name = result.name ?? ""
            let icon_name = result.weather.first?.icon ?? ""
            let weather_status = result.weather.first?.main ?? ""
            let temp = result.main.temp ?? 0
            let humidity = result.main.humidity ?? 0
            let wind_speed = result.wind.speed ?? 0
            
            tempLabelFlag.text = "0"
            attributedString.append(NSAttributedString(string: String(format: "%@\n\n", city_name), attributes: [
                NSAttributedString.Key.font : FontFamily.Roboto.medium.font(size: 25)!,
                NSAttributedString.Key.foregroundColor : UIColor.white,
                ]))
            attributedString.append(NSAttributedString(string: weather_status, attributes: [
                NSAttributedString.Key.font : FontFamily.Roboto.medium.font(size: 15)!,
                NSAttributedString.Key.foregroundColor : UIColor.white,
                ]))
            cityLabel.attributedText = attributedString
            cityLabel.numberOfLines = 0
            
            if let icon_url = URL(string: String(format: "http://openweathermap.org/img/wn/%@.png", icon_name)) {
                weatherIcon.af_setImage(withURL: icon_url)
            }
            
            tempLabel.font = FontFamily.Roboto.medium.font(size: 60)!
            tempLabel.text = String(format: "%i", Int(temp))
            
            attributedString = NSMutableAttributedString()
            attributedString.append(NSAttributedString(string: String(format: "%i mph\n", Int(wind_speed)), attributes: [
                NSAttributedString.Key.font : FontFamily.Roboto.medium.font(size: 20)!,
                NSAttributedString.Key.foregroundColor : UIColor.white,
                ]))
            attributedString.append(NSAttributedString(string: "Wind Speed", attributes: [
                NSAttributedString.Key.font : FontFamily.Roboto.regular.font(size: 17)!,
                NSAttributedString.Key.foregroundColor : UIColor(hex: "cccccc"),
                ]))
            windLabel.attributedText = attributedString
            windLabel.numberOfLines = 0
            
            attributedString = NSMutableAttributedString()
            attributedString.append(NSAttributedString(string: String(format: "%i %\n", humidity), attributes: [
                NSAttributedString.Key.font : FontFamily.Roboto.medium.font(size: 20)!,
                NSAttributedString.Key.foregroundColor : UIColor.white,
                ]))
            attributedString.append(NSAttributedString(string: "Humidity", attributes: [
                NSAttributedString.Key.font : FontFamily.Roboto.regular.font(size: 17)!,
                NSAttributedString.Key.foregroundColor : UIColor(hex: "cccccc"),
                ]))
            humidityLabel.attributedText = attributedString
            humidityLabel.numberOfLines = 0
            
            wrapperMapButton.layer.cornerRadius = 10
            wrapperMapButton.layer.shadowOpacity = 0.7
            wrapperMapButton.layer.shadowRadius = CGFloat(4)
            wrapperMapButton.layer.shadowOffset = CGSize.init(width: 1, height: 2)
            wrapperMapButton.layer.shouldRasterize = true

            wrapperMapButton.layer.shadowColor = UIColor.black.cgColor

        }
        
        
    }
    
    func getCurrentWeather(){
        indicatorView.startAnimating()
        if let lat = latitude, let lon = longitude {
            MainViewModel.getCurrentWeather(params: ["lat": lat, "lon": lon, "units": (isCelsius ? "metric" : "imperial")], success: { (result) in
                self.mainWeather = MainWeather(result)
                self.setupUI()
                self.indicatorView.stopAnimating()
                
            }) { (error)  in
                print("error is \(error ?? "")")
                //            self.showAlert("Error", error!, "OK", false)
                self.indicatorView.stopAnimating()
                
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    

}


extension DashboardViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if(locValue.latitude != latitude && locValue.longitude != longitude){
            latitude = locValue.latitude
            longitude = locValue.longitude
            getCurrentWeather()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locations error = \(error.localizedDescription)")
        
    }
}
