//
//  MainViewModel.swift
//  MyWeather
//
//  Created by iOS on 17/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import UIKit
import SwiftyJSON

class MainViewModel: NSObject {
    static func getCurrentWeather(params: [String:Any], success: @escaping (JSON)->Void, failed: @escaping (String?)->Void) {
        ApiHelper.getRequest(requestName: "data/2.5/weather", params: params, success:  { (result) in
            success(result)
        }) { (error, errorCode)  in
            failed(error)
        }
    }
}

