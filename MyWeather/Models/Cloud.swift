//
//  Cloud.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 17, 2020

import Foundation
import SwiftyJSON


class Cloud : NSObject, NSCoding{

    var all : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    override init(){
        super.init()
    }
    
    
    init(_ json: JSON!){
		if json.isEmpty{
			return
		}
        all = json["all"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if all != nil{
        	dictionary["all"] = all
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		all = aDecoder.decodeObject(forKey: "all") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if all != nil{
			aCoder.encode(all, forKey: "all")
		}

	}

}
