//
//  ApiHelper.swift
//  MyWeather
//
//  Created by iOS on 17/01/20.
//  Copyright © 2020 ios developer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


public typealias ParamClosure = [String: Any]?
public typealias SuccessClosure = (_ data:[String:AnyObject]) -> Void
public typealias FailedClosure = (_ error:[String:AnyObject]) -> Void
public typealias StringClosure = (_ error:String) -> Void

class ApiHelper: NSObject {
    
    static let instance = ApiHelper()
    
    //MARK: - Base Header
    
    override init() {
        super.init()
    }
    
    static let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        let delegate = SessionDelegate()
        
        return Alamofire.SessionManager(configuration: configuration, delegate: delegate, serverTrustPolicyManager: nil)
    }()
    
    static func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
    
    static func getRequest(requestName: String, params: [String:Any], success: @escaping(JSON) -> Void, failed: @escaping (String,Int)->Void) {
        let urlRequest = Constants.ROOT_URL+"/"+requestName
        var parameters = params
        parameters["appid"] = "fdf871cedaf3413c6a23230372c30a02"
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("parameters \(parameters)")
        Alamofire.request(urlRequest, method: .get, parameters: parameters).responseJSON { response in
            print("RESPONSE \(urlRequest) \(String(describing: response.result.value))")
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 0
            
            switch response.result {
            case .failure(let error):
                failed(error.localizedDescription, statusCode)
            case .success(let value):
                let json = JSON(value)
                
                if statusCode == 200 {
                    
                    success(json)
                } else {
                    
                    let message = json["message"].stringValue
                    failed(message, statusCode)
                    
                }
            }
        }
    }
}

